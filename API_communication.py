#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  5 16:04:26 2021

@author: augusto
"""

import threading as th
import queue as q
import socket
import os

########################## server ##################
class Server:

    def __init__(self, host = "localhost", port = 8080, string_len = 150):
        """
        start the object connect

        'host' is the host with the ip of the server
        'port' is the port to connect in the server receiver
        'string_len' is the size of the string
        """

        self.host = host
        self.port_rcv = port
        self.client = ""
        self.server = socket.socket()
        self.string_max_size = string_len
        self.addr = ""

    def init_server(self):
        '''

        start the IP how server in the port selected

        '''
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((self.host, self.port_rcv))

    def connection_accept(self):
        '''
        acept any connection avaible in port on host
        '''
        print("waiting ")
        self.server.listen()
        (self.client, self.addr) = self.server.accept()
        print( "connected:", self.addr,
               "port_rcv:", self.port_rcv )
        return (self.client, "0")

    def receiver(self):
        '''

        receive any message avaible of a sender, and decodes

        '''

        string = self.client.recv(self.string_max_size+1)
        # print(self.string_max_size)
        string = string.decode('utf-8')
        if string != "":
            # print("\nreceived: "+string+"\nsize: "+str(len(string)))
            string = string.replace("0","")
            string = string.replace("+","0")
            return string

    def close(self, string=""):
        """

        close connection with any client

        """
        self.client.close()
        self.addr = string

    def as_connected(self):
        """

        verify if as connected

        """
        if self.addr != "":
            return True
        else:
            return False

class Receiver_Methods:

    def __init__(self, host = "localhost", port = 8080, string_len = 150 ):

        self.cmd= q.Queue()
        self.close = False
        self.exit = False
        self.is_sender = False
        self.server = Server(host = host, port = port, string_len = string_len)
        self.server.init_server()
        (self.client, self.last_cmd) = self.server.connection_accept()


    def start(self):
        '''
        start main program to server
        to use this need start in a thread
        '''

        while self.last_cmd != "&Xi7":
            if self.last_cmd == "" and self.close:
                (self.client, self.last_cmd) = self.server.connection_accept()
                self.close = False
            # print("recebendo ")
            if self.last_cmd == "888%74":
                self.server.close(self.client,"waiting for any client")
                self.last_cmd = ""
                self.close = True
                continue

            self.receiver()
        self.exit = True
        self.server.close(self.client)
        self.last_cmd = ""

    def receiver(self):

        self.last_cmd = self.server.receiver()
        if not self.last_cmd == "0":
            if self.last_cmd != "&Xi7" and self.last_cmd != "888%74":
                self.cmd.put(self.last_cmd)

    def get_cmd(self):

        return self.cmd.get()

class Receiver:

    def __init__(self, host = "localhost", port = 8080, string_len = 150):

        try:
            self.host = str(os.environ['host'])
        except :
            self.host = host
        try:
            self.port = str(os.environ['port'])
        except :
            self.port = port
        try:
            self.string_len = str(os.environ['stringlen'])
        except :
            self.string_len = string_len

        self.receiver = Receiver_Methods( host = self.host,
                                          port = self.port,
                                          string_len = self.string_len )

        th.Thread(target = self.receiver.start).start()

    def restart(self):

        return th.Thread( target = self.receiver.start).start()

    def get_cmd(self):
        cmd = self.receiver.get_cmd()
        return cmd

    def cmd_empty(self):
        return self.receiver.cmd.empty()

    def as_connected(self):
        return self.receiver.server.as_connected()

############# client ##############

class Client:

    host = ""

    def __init__(self, host = "localhost", port = 8080, string_len = 150):
        '''
        start the object connect

        'host' is the host with the ip of the server

        'port' is the port to connect in the server

        'string_len' is the size of the string

        '''
        self.host = host
        self.string_max_size = string_len
        self.port_send = port
        self.rcv = socket.socket()
        self.erro = "0000"

    def connect(self):
        '''
        connect a host
        '''
        try:
            self.rcv = socket.socket()
            self.rcv.connect((self.host, self.port_send))
            self.erro = ""
        except Exception as erro:
            if str(erro) != self.erro:
                self.erro = str(erro)
                print(self.erro)


    def sender(self, string):
        '''
        send the string to a any listener connected
        @param 'string', is the command for send
        '''
        st = "".zfill(int(self.string_max_size)-len(string))
        string = string.replace("0","+")
        string = string+st
        # print("\nsendered: "+string+"\nsize: "+str(len(string)))
        try:
            self.rcv.send(string.encode('utf-8'))
        except Exception as erro:
            self.erro = str(erro)
            print(self.erro)


    def as_connected(self):
        """

        verify if as connected

        """
        if self.erro == "":
            return True
        else:
            return False

class Sender_Methods:

    def __init__(self, host = "localhost", port = 8080, string_len = 150):
        '''
        start the object connect

        'host' is the host with the ip of the server

        'port' is the port to connect in the server

        'string_len' is the size of the string

        '''
        try:
            self.host = str(os.environ['host'])
        except :
            self.host = host
        try:
            self.port_send = str(os.environ['port'])
        except :
            self.port_send = port

        self.sender = Client ( host = self.host,
                               port = self.port_send,
                               string_len = string_len )
        self.cmds_to_send = q.Queue()

    def start_send(self):
        '''
        this start the process to send any message in the queue
        '''
        # print("tentando enviar")
        if not self.sender.as_connected():

            print("trying connect")

            while not self.sender.as_connected():
                self.sender.connect()

            print("connected")

        while self.sender.as_connected() or not self.cmds_to_send.empty():

            if not self.sender.as_connected():

                print("trying connect")

                while not self.sender.as_connected():
                    self.sender.connect()

                print("connected")
                continue

            elif not self.cmds_to_send.empty():
                string = self.cmds_to_send.get()
                self.sender.sender(string)
                if string == "888%74" or string == "&Xi7":
                    self.sender.erro = "anything"
                    break
                # print("enviado", self.erro)

    def send_cmd(self, string):
        '''

        take a string in this program
        @return 'string', take the command received

        '''
        self.cmds_to_send.put(string)

class Sender:

    def __init__(self, host = "localhost", port = 8080, string_len = 150):

        try:
            self.host = str(os.environ['host'])
        except :
            self.host = host
        try:
            self.port = str(os.environ['port'])
        except :
            self.port = port
        try:
            self.string_len = str(os.environ['stringlen'])
        except :
            self.string_len = string_len

        self.sender = Sender_Methods ( host = self.host,
                                       port = self.port,
                                       string_len = self.string_len )

        th.Thread(target=self.sender.start_send).start()

    def reconnect(self):
        th.Thread(target=self.sender.start_send).start()

    def close_connection(self):
        self.sender.send_cmd("888%74")

    def exit_and_kill(self):
        self.sender.send_cmd("&Xi7")

    def send_cmd(self, string):
        self.sender.send_cmd(string)

    def as_connected(self):
        return self.sender.sender.as_connected()

######################## comunication ############################

class Machine:

    def __init__( self,
                  is_first:bool,
                  ip_machine_1,
                  ip_machine_2,
                  port_1,
                  port_2,
                  string_len ):

        if is_first:
            self.rcv = Receiver(ip_machine_1, port_1, string_len)
            self.snd = Sender(ip_machine_2, port_2, string_len)
        else:
            self.snd = Sender(ip_machine_2, port_2, string_len)
            self.rcv = Receiver(ip_machine_1, port_1, string_len)

        th.Thread(target=self.is_to_exit).start()

    def is_to_exit(self):
        while not self.rcv.receiver.exit and not self.rcv.receiver.close:
            pass
        if not self.rcv.receiver.is_sender:
            if self.rcv.receiver.exit:
                self.snd_exit_and_kill()

            if self.rcv.receiver.close:
                self.snd_close_connection()

    def rcv_restart(self): # ok
        self.rcv.restart()

    def rcv_get_cmd(self): # ok
        cmd = self.rcv.get_cmd()
        return cmd

    def rcv_cmd_empty(self): # ok
        return self.rcv.cmd_empty()

    def rcv_as_connected(self): # ok
        return self.rcv.as_connected()

    def snd_reconnect(self): # ok
        self.snd.reconnect()

    def snd_close_connection(self, is_sender=False): # ok
        self.rcv.receiver.is_sender = is_sender
        self.snd.close_connection()

    def snd_exit_and_kill(self, is_sender=False): # ok
        self.rcv.receiver.is_sender = is_sender
        self.snd.exit_and_kill()

    def snd_send_cmd(self, string): # ok
        self.snd.send_cmd(string)

    def snd_as_connected(self): # ok
        return self.snd.as_connected()

############## o que será usado na interface ###################
class Communication:

    def __init__( self,
                  first_machine:bool,
                  ip_machine_1:str,
                  ip_machine_2:str,
                  port_1:int,
                  port_2:int,
                  string_len:int ):
        '''

        classe de comunicação a ser utilizada por demais

        Parameters
        ----------
        first_machine : bool
            se esta maquina vai iniciar primeiro ou não.
        ip_machine_1 : str
            o primeiro ip.
        ip_machine_2 : str
            o segundo ip.
        port_1 : int
            a primeira porta.
        port_2 : int
            a segunda porta.
        string_len : int
            o tamanho maximo de mensagem a ser enviada.

        Returns
        -------
        None.

        '''
        if first_machine:
            self.machine = Machine( first_machine,
                                    ip_machine_1,
                                    ip_machine_2,
                                    port_1,
                                    port_2,
                                    string_len)
        else:
            self.machine = Machine( first_machine,
                                    ip_machine_2,
                                    ip_machine_1,
                                    port_2,
                                    port_1,
                                    string_len)


    def sender_as_connected(self):
        """


        Returns
        -------
        bool
            se houver alguma conexão apartir de um mensageiro com recebedor
            retorna verdadeiro se não retorna falso.

        """
        return self.machine.snd_as_connected()

    def receiver_as_connected(self):
        """


        Returns
        -------
        bool
            se houver alguma conexão apartir de um recebedor
            com algum mensageiro retorna verdadeiro se não retorna falso.

        """
        return self.machine.rcv_as_connected()

    def send_cmd(self, string:str):
        '''

        envia um comando para o recebedor conectado

        Parameters
        ----------
        string : str
            comando a ser enviado.

        Returns
        -------
        None.

        '''
        self.machine.snd_send_cmd(string)

    def get_cmd(self):
        '''


        Returns
        -------
        str
            retorna apartir da fila o comando que recebeu.

        '''
        return self.machine.rcv_get_cmd()

    def cmd_empty(self):
        '''


        Returns
        -------
        bool
            retorna True se a fila de comandos está vazia, se não False.

        '''
        return self.machine.rcv_cmd_empty()

    def restart_receiver(self):
        '''
        reinicia o recebedor.

        Returns
        -------
        None.

        '''
        self.machine.rcv_restart()

    def reconnect_sender(self):
        '''
        reconecta o mensageiro

        Returns
        -------
        None.

        '''
        self.machine.snd_reconnect()

    def close(self):
        '''

        fecha a conexão em ambas as maquinas, para reconectar é necessario
        reconectar cada uma manualmente.

        Nota: o recebedor fica ativo em espera por uma conexão

        Returns
        -------
        None.

        '''

        self.machine.snd_close_connection(True)

    def exit_all(self):
        '''
        fecha a conexão em ambas as maquinas, para reconectar é necessario
        reiniciar o recebedor, e reconectar o mensageiro manualmente

        Returns
        -------
        None.

        '''

        self.machine.snd_exit_and_kill(True)
